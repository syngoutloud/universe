﻿Shader "Custom/TranspUnlit2" {
      Properties {
          _Color("Color & Transparency", Color) = (0, 0, 0, 0.5)
          _MainTex ("Texture", 2D) = "white" {} 
      }
      SubShader {
          Lighting Off
          ZWrite Off
          Cull Front
          Blend SrcAlpha OneMinusSrcAlpha
          Tags { "Queue"="Transparent" "RenderType"="Transparent" "IgnoreProjector"="True" }
          Color[_Color]
          Pass {
            
              SetTexture [_MainTex]{
                  combine texture * primary
              }
              
          }
      } 
      FallBack "Unlit/Transparent"
  }