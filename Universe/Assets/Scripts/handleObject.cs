﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleObject : MonoBehaviour
{
    bool isPressed = false;

    public GameObject closestRoom;

    public GameObject prevClosestRoom;

    GameObject[] allRooms;

    float distanceToDrawerIcon;

    handleState HandleState;

    bool isPlayingInMultipleRooms = false;


    // Start is called before the first frame update
    void Start()
    {

        Application.targetFrameRate = 60;

        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();

        allRooms = GameObject.FindGameObjectsWithTag("room");

        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;

        GetComponent<PressGesture>().StateChanged += pressHandler;

        

    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed == true)
        {
            distanceToDrawerIcon = Vector3.Distance(gameObject.transform.position, GameObject.Find("drawerIcon").transform.position);
            //print("distt = " + distanceToDrawerIcon);
        }
    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {

            if (prevClosestRoom != closestRoom)
            {
                prevClosestRoom = closestRoom;
            }

            findNearestRoom();
            setParentRoom();
            isPressed = false;

            

            if (closestRoom.name == "roomPlayEverywhere")
            {
                if (isPlayingInMultipleRooms == false)
                {
                    isPlayingInMultipleRooms = true;
                    StartCoroutine(HandleState.addPlaybackToggleToAllRooms(prevClosestRoom));
                }
                
                snapToRoomPlayEverywhere();
               

            }
            else
            {
                if (isPlayingInMultipleRooms == true)
                {
                    isPlayingInMultipleRooms = false;
                    removePlaybackToggles();
                }


            }

            print("closest = " + closestRoom);
            print("prevClosest = " + prevClosestRoom);

        }

    }

    void removePlaybackToggles()
    {
        float delay = 0;

        foreach (GameObject playbackToggle in GameObject.FindGameObjectsWithTag("playbackToggle"))
        {
            delay += 0.05f;
            playbackToggle.transform.DOMove(gameObject.transform.position, 0.3f).SetDelay(delay);
            Destroy(playbackToggle, 0.5f);
        }
    }

    void snapToRoomPlayEverywhere()
    {
        transform.DOLocalMove(new Vector3(0, 0, 0), 0.2f).SetEase(Ease.OutBack);
    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {

            isPressed = true;

        }

    }

    void setParentRoom()
    {
        transform.parent = closestRoom.transform;
    }

    public void findNearestRoom()
    {

        

        float distanceToClosestRoom = Mathf.Infinity;
        closestRoom = null;

        foreach (GameObject room in allRooms)
        {

            float distanceToRoom = (room.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToRoom < distanceToClosestRoom)
            {
                distanceToClosestRoom = distanceToRoom;
                closestRoom = room;
            }

        }

        Debug.DrawLine(this.transform.position, closestRoom.transform.position);

    }

}
