﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;

public class handleGlobeGestures : MonoBehaviour
{

    private TransformGesture tranformGesture;
    private PressGesture pressGesture;
    private ReleaseGesture releaseGesture;
    float panScale = 3;
    bool isPressed = false;
    Vector3 transformDeltaPos;

    // Start is called before the first frame update
    void Start()
    {

        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;

        tranformGesture = GetComponent<TransformGesture>();
        tranformGesture.Transformed += transformedHandler;

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            isPressed = true;

            transformDeltaPos = new Vector3(0, 0, 0);

            gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);

            //print("press");
        }

    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            isPressed = false;

            gameObject.GetComponent<Rigidbody>().AddTorque(new Vector3(0, -transformDeltaPos.x * panScale, 0), ForceMode.Impulse);

            //print("release");
        }

    }

    private void transformedHandler(object sender, System.EventArgs e)
    {


        if (isPressed == true)
        {
            transformDeltaPos = tranformGesture.DeltaPosition;

            gameObject.transform.eulerAngles += new Vector3(0, -transformDeltaPos.x * panScale, 0);
        }

        


    }

}

    
