﻿//NOICE https://forum.unity.com/threads/distributing-empty-gameobjects-evenly-on-a-sphere.215479/

using UnityEngine;
using System.Collections;
using TMPro;
using TouchScript;

public class SphereMaker : MonoBehaviour
{

    public int numberOfPoints = 300;
    public float scale = 1.0f;
    public Material dotMaterialLit;
    public Material dotMaterialUnlit;
    public Material globeMaterialOuter;
    public Material globeMaterialInner;
    int dotNumber;
    public GameObject labelHome;
    public GameObject Rooms;
    public Material earthGlobe;
    

    

    // Use this for initialization
    void Start()
    {
        dotNumber = 0;

        GameObject innerSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        innerSphere.transform.localScale = innerSphere.transform.localScale * (scale * 20);
        innerSphere.transform.name = "Inner Sphere";
        innerSphere.transform.parent = gameObject.transform;
        innerSphere.GetComponent<MeshRenderer>().material = globeMaterialOuter;

        GameObject innerSphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        innerSphere2.transform.localScale = innerSphere2.transform.localScale * (scale * 20);
        innerSphere2.transform.name = "Inner Sphere Back";
        innerSphere2.transform.parent = gameObject.transform;
        innerSphere2.GetComponent<MeshRenderer>().material = globeMaterialInner;

        Vector3[] myPoints = GetPointsOnSphere(numberOfPoints);

        foreach (Vector3 point in myPoints)
        {
            dotNumber += 1;
            GameObject outerSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            outerSphere.name = "sphereDot" + dotNumber.ToString();
            outerSphere.transform.position = point * (scale*10);
            outerSphere.transform.parent = gameObject.transform;

            if (dotNumber == 249)
            {
                outerSphere.GetComponent<MeshRenderer>().material = dotMaterialLit;
                outerSphere.transform.localScale = outerSphere.transform.localScale * (scale / 1.5f);
                outerSphere.GetComponent<SphereCollider>().isTrigger = true;
                outerSphere.GetComponent<SphereCollider>().radius = 4;
                outerSphere.AddComponent<TouchScript.Gestures.PressGesture>();
                outerSphere.AddComponent<TouchScript.Gestures.ReleaseGesture>();
                outerSphere.AddComponent<handleButton>();
                outerSphere.transform.localPosition = new Vector3(-3.72f, 5.92f, -7.33f); //hack to position it over LA
                outerSphere.tag = "dotPlace";
                labelHome.transform.parent = outerSphere.transform;
                labelHome.transform.localPosition = new Vector3(0, 0, 0);
                Rooms.transform.parent = outerSphere.transform;
                Rooms.transform.localPosition = new Vector3(0, 0.039f, 0);

            }
            else
            {
                outerSphere.GetComponent<MeshRenderer>().material = dotMaterialUnlit;
                //outerSphere.transform.localScale = outerSphere.transform.localScale * (scale / 5);
                outerSphere.transform.localScale = outerSphere.transform.localScale * 0; //hack so we only show one dot
            }

            
        }

    }

    Vector3[] GetPointsOnSphere(int nPoints)
    {
        float fPoints = (float)nPoints;

        Vector3[] points = new Vector3[nPoints];

        float inc = Mathf.PI * (3 - Mathf.Sqrt(5));
        float off = 2 / fPoints;

        for (int k = 0; k < nPoints; k++)
        {
            float y = k * off - 1 + (off / 2);
            float r = Mathf.Sqrt(1 - y * y);
            float phi = k * inc;

            points[k] = new Vector3(Mathf.Cos(phi) * r, y, Mathf.Sin(phi) * r);
        }

        return points;
    }

}