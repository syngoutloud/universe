﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class handleButton : MonoBehaviour
{

  

    Vector3 startingScale;
    float pressScale = 0.95f;
    float pressDuration = 0.05f;
    float releaseDuration = 0.25f;
 
    handleState HandleState;

    public Vector3 longPressActivatedPos;



    // Use this for initialization
    void Start()
    {

        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();

        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;

        GetComponent<PressGesture>().StateChanged += pressHandler;

        if (GetComponent<LongPressGesture>() != null)
        {
            GetComponent<LongPressGesture>().StateChanged += longPressHandler;
        }

        startingScale = transform.localScale;

    }

    private void longPressHandler(object sender, GestureStateChangeEventArgs e)
    {

        

        LongPressGesture gesture = sender as LongPressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            // print("longpress!");

           

            if (gesture.gameObject.tag == "object")
            {
                longPressActivatedPos = gesture.gameObject.transform.position;
                //print("pos = " + longPressActivatedPos);
                HandleState.addToAllRoomsSpawnPos = longPressActivatedPos;
            }

            //longPressActivatedPos = new Vector3(0, 0, 0);

            if (HandleState.contextMenuIsHidden == true)
            {
                
                HandleState.contextMenuIsHidden = false;
                HandleState.showHideContextMenu();
            }


            

        }

    }



    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            print("released "+ gesture.gameObject.name);

            //buttonUp.Play();

            //gameObject.GetComponent<SpriteRenderer>().transform.DOScaleX(startingScale.x, releaseDuration).SetEase(Ease.OutBack);
            //gameObject.GetComponent<SpriteRenderer>().transform.DOScaleY(startingScale.y, releaseDuration).SetEase(Ease.OutBack);
            //gameObject.GetComponent<SpriteRenderer>().transform.DOScaleZ(startingScale.z, releaseDuration).SetEase(Ease.OutBack);

 
                if (gesture.gameObject.tag == "dotPlace")
                {
                   // print("released dotPlace");

                    HandleState.incrementZoomLevel();


                }

                if (gesture.gameObject.tag == "backButton")
                {

                    //print("released backButton");

                    HandleState.decrementZoomLevel();

                }

                if (gesture.gameObject.tag == "room")
                {

                    if (HandleState.contextMenuIsHidden == true)
                    {

                        print("released a room");

                        HandleState.selectedRoom = gesture.gameObject;

                        if (HandleState.zoomLevel < HandleState.numberOfZoomLevels-1)
                        {
                            HandleState.incrementZoomLevel();
                        }

                    }

                }

                

                if (gesture.gameObject.tag == "object")
                {


                    foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
                    {
                        room.GetComponent<PressGesture>().enabled = true;
                        room.GetComponent<ReleaseGesture>().enabled = true;
                    }



                }
           

            

            

            if (gesture.gameObject.tag == "closeX")
            {

               // print("released closeX");

                if (HandleState.contextMenuIsHidden == false)
                {
                    HandleState.contextMenuIsHidden = true;
                    HandleState.showHideContextMenu();
                }

            }

            if (gesture.gameObject.tag == "addToAllRooms")
            {

                print("released addToAllRooms");


                HandleState.contextMenuIsHidden = true;
                HandleState.showHideContextMenu();

                print("possss=" + longPressActivatedPos);

                //StartCoroutine(HandleState.addObjectToAllRooms());


            }

            if (gesture.gameObject.tag == "playbackToggle")
            {
                //print("RELEASED toggle playback");

                if (HandleState.zoomLevel == 1) //place
                {
                    gesture.gameObject.GetComponent<handlePlaybackToggle>().togglePlayback();

                    foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
                    {
                        room.GetComponent<PressGesture>().enabled = true;
                        room.GetComponent<ReleaseGesture>().enabled = true;
                    }
                }

                
            }

        }

    }

   



    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("button pressed!");

            //buttonDown.Play();

            //gameObject.GetComponent<SpriteRenderer>().transform.DOScaleX(startingScale.x * pressScale, pressDuration);
            //gameObject.GetComponent<SpriteRenderer>().transform.DOScaleY(startingScale.y * pressScale, pressDuration);
            //gameObject.GetComponent<SpriteRenderer>().transform.DOScaleZ(startingScale.z * pressScale, pressDuration);

            if (gesture.gameObject.tag == "dotPlace")
            {
                //print("pressed dotPlace");

            }

            if (gesture.gameObject.tag == "backButton")
            {
               // print("pressed backButton");

            }

            if (gesture.gameObject.tag == "object")
            {

                print("pressed an object");

                foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
                {
                    room.GetComponent<PressGesture>().enabled = false;
                    room.GetComponent<ReleaseGesture>().enabled = false;
                }


            }



            if (gesture.gameObject.tag == "room")
            {

                print("pressed room");


            }

            if (gesture.gameObject.tag == "addToAllRooms")
            {

                print("pressed addToAllRooms");



            }

            if (gesture.gameObject.tag == "playbackToggle")
            {
                //print("PRESSED toggle playback");

                if (HandleState.zoomLevel == 1) //place
                {
                    foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
                    {
                        room.GetComponent<PressGesture>().enabled = false;
                        room.GetComponent<ReleaseGesture>().enabled = false;
                    }
                }

                    
            }

        }

    }

}



