﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Gestures;

public class handleState : MonoBehaviour
{
   public int zoomLevel;
    public int numberOfZoomLevels;
    Camera mainCamera;
    Vector3 camStartPos;
    public GameObject labelHome;
    float cameraZOffset;
    float cameraYOffset;
    public GameObject globe;
    public Material sphereDotMaterialUnlit;
    public Material sphereDotMaterialLit;
    public GameObject labelBackButton;
    public TextMeshPro labelTitle;
    bool weAreZoomingIn = false;
    GameObject homeSphere;
    public GameObject selectedRoom;
    GameObject innerSphere;
    GameObject innerSphereBack;
    float transitionDuration;
    Ease easeType;
    public GameObject drawerIcon;
    float contextMenuVisibleYPos;
    float contextMenuHiddenYPos;
    public bool contextMenuIsHidden = true;
    public GameObject contextMenu;
    public GameObject iconDrawer;
    public GameObject contextMenuBkg;
    public GameObject contextMenuHiddenCloseButton;
    public GameObject objectPrefab;
    public GameObject playbackTogglePrefab;
    Color globeFrontOriginalColor;
    Color globeBackOriginalColor;
    public Vector3 addToAllRoomsSpawnPos;
    public TextMeshPro labelAddToAllRooms;
    public TextMeshPro labelTakeOverAllRooms;
    public GameObject iconPlayEverywhere;
    public GameObject bkgPlayEverywhere;


    // Start is called before the first frame update
    void Start()
    {

        contextMenuVisibleYPos = -0.7f;
        contextMenuHiddenYPos = -2.542f;

        transitionDuration = 1f;
        sphereDotMaterialUnlit.DOFade(1, 0);
        sphereDotMaterialLit.DOFade(1, 0);

        zoomLevel = 0;
        numberOfZoomLevels = 3;
        mainCamera = Camera.main;
        cameraZOffset = 1.2f;
        cameraYOffset = 0.05f;
        camStartPos = mainCamera.transform.position;

        homeSphere = GameObject.FindGameObjectWithTag("dotPlace");

        innerSphere = GameObject.Find("Inner Sphere");
        innerSphereBack = GameObject.Find("Inner Sphere Back");

        globeFrontOriginalColor = innerSphere.GetComponent<MeshRenderer>().material.color;
        globeBackOriginalColor = innerSphereBack.GetComponent<MeshRenderer>().material.color;

        foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
        {

            room.GetComponent<SpriteRenderer>().DOFade(1, 0);
        }

        foreach (GameObject roomLabel in GameObject.FindGameObjectsWithTag("roomLabel"))
        {
            roomLabel.GetComponent<TextMeshPro>().DOFade(0, 0);
        }
    }

    


    public IEnumerator addPlaybackToggleToAllRooms(GameObject prevRoom)
    {


        var allRooms = new List<GameObject>();

        foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
        {
            //make a list of all rooms except the play everywhere one
            if (room.name != "roomPlayEverywhere" && room.name != "roomPlayEverywhereBkg")
            {
                allRooms.Add(room);
            }
            
            
            
        }

        yield return new WaitForSeconds(0.1f);

        foreach (GameObject room in allRooms)
        {
            ////instantiate toggle prfabs for each room, then animate them moving there

            var newObject = Instantiate(playbackTogglePrefab, iconPlayEverywhere.transform.position, Quaternion.identity);

            newObject.transform.parent = room.transform;

            newObject.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);

            newObject.transform.DOMove(new Vector3(room.transform.position.x, room.transform.position.y, room.transform.position.z), 0.3f).SetEase(Ease.OutBack);

            //turn on the playback toggle for the room we just left
            if (room == prevRoom)
            {
                newObject.GetComponent<handlePlaybackToggle>().playbackIsEnabled = true;
                newObject.GetComponent<handlePlaybackToggle>().setIconState();
            }

            
            


        }


        


    }



    public void showHideContextMenu()
    {
        if (contextMenuIsHidden == true)
        {
            contextMenuHiddenCloseButton.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            contextMenu.transform.DOLocalMoveY(contextMenuHiddenYPos, 0.4f).SetEase(Ease.InSine);
            iconDrawer.GetComponent<SpriteRenderer>().DOFade(1, 0.3f).SetDelay(0.2f);
            contextMenuBkg.GetComponent<SpriteRenderer>().DOFade(0, 0.3f);

            foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
            {
                room.GetComponent<BoxCollider>().enabled = true;
            }

            foreach (GameObject Object in GameObject.FindGameObjectsWithTag("object"))
            {
                Object.GetComponent<SphereCollider>().enabled = true;
            }

        }
        else
        {
            contextMenuHiddenCloseButton.transform.localScale = new Vector3(1, 1, 1);
            contextMenu.transform.DOLocalMoveY(contextMenuVisibleYPos, 0.4f).SetEase(Ease.OutCirc);
            iconDrawer.GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
            contextMenuBkg.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);

            foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
            {
                room.GetComponent<BoxCollider>().enabled = false;
            }

            foreach (GameObject Object in GameObject.FindGameObjectsWithTag("object"))
            {
                Object.GetComponent<SphereCollider>().enabled = false;
            }
        }
    }

    void setCameraPerZoomLevel()
    {
        //when we increment/decrement zoom level, we move the camera to its respective target.

        

        switch (zoomLevel)
        {
            case 0: //GLOBE//////////////////////////////////////////////////////////////////////////

                innerSphere.SetActive(true);
                innerSphereBack.SetActive(true);

                innerSphere.GetComponent<SphereCollider>().enabled = false;
                innerSphereBack.GetComponent<SphereCollider>().enabled = false;

                easeType = Ease.OutExpo;
                transitionDuration = 0.8f;

                globe.GetComponent<Rigidbody>().freezeRotation = false;
                mainCamera.transform.DOMove(camStartPos, transitionDuration);
                labelHome.GetComponent<TextMeshPro>().DOFade(1, transitionDuration/2).SetDelay(transitionDuration * 0.3f);
                labelBackButton.GetComponent<TextMeshPro>().DOFade(0, transitionDuration * 0.5f);
               
                sphereDotMaterialUnlit.DOFade(1, transitionDuration);
                sphereDotMaterialLit.DOFade(1, transitionDuration);

                homeSphere.GetComponent<PressGesture>().enabled = true;
                homeSphere.GetComponent<ReleaseGesture>().enabled = true;

                globe.GetComponent<SphereCollider>().enabled = true;

                innerSphere.GetComponent<MeshRenderer>().material.DOColor(globeFrontOriginalColor, transitionDuration);
                innerSphereBack.GetComponent<MeshRenderer>().material.DOColor(globeBackOriginalColor, transitionDuration);


                //fade out the rooms

                foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
                {
                    
                        room.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration/2);

                        room.GetComponent<BoxCollider>().enabled = true;
                    
                }

                foreach (GameObject roomLabel in GameObject.FindGameObjectsWithTag("roomLabel"))
                {

                    roomLabel.GetComponent<TextMeshPro>().DOFade(0, transitionDuration / 2);


                }

                drawerIcon.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration / 2).SetEase(easeType);
                iconPlayEverywhere.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration / 2).SetEase(easeType);


                break;

            case 1: //PLACE//////////////////////////////////////////////////////////////////////////



                if (weAreZoomingIn == true)
                {
                    easeType = Ease.InOutQuad;
                    transitionDuration = 1.2f;
                }
                else
                {
                    easeType = Ease.OutExpo;
                    transitionDuration = 0.8f;
                    //Invoke("disableInnerSphere", transitionDuration);
                }


               

                globe.GetComponent<Rigidbody>().freezeRotation = true;
                mainCamera.transform.DOMove(new Vector3(labelHome.transform.parent.transform.position.x, labelHome.transform.parent.transform.position.y-cameraYOffset, labelHome.transform.parent.transform.position.z-cameraZOffset), transitionDuration).OnComplete(resetLookAtTheCamera).SetEase(easeType);
                labelHome.GetComponent<TextMeshPro>().DOFade(0, transitionDuration / 4);
                
                sphereDotMaterialUnlit.DOFade(0, transitionDuration * 1.5f);
                sphereDotMaterialLit.DOFade(0, transitionDuration * 0.75f);

                homeSphere.GetComponent<PressGesture>().enabled = false;
                homeSphere.GetComponent<ReleaseGesture>().enabled = false;

                globe.GetComponent<SphereCollider>().enabled = false;

                innerSphere.GetComponent<SphereCollider>().enabled = false;
                innerSphereBack.GetComponent<SphereCollider>().enabled = false;

                innerSphere.GetComponent<MeshRenderer>().material.DOColor(Color.black, transitionDuration * 0.75f).SetDelay(transitionDuration * 0.25f);
                innerSphereBack.GetComponent<MeshRenderer>().material.DOColor(Color.black, transitionDuration * 0.75f).SetDelay(transitionDuration * 0.25f);



                if (selectedRoom != null)
                {
                    selectedRoom.GetComponent<PressGesture>().enabled = true;
                    selectedRoom.GetComponent<ReleaseGesture>().enabled = true;
                }



                //fade in the rooms

                foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
                {
                    
                    room.GetComponent<SpriteRenderer>().DOFade(1, transitionDuration).SetEase(easeType);
                    room.GetComponent<BoxCollider>().enabled = true;
                    
                }

                bkgPlayEverywhere.GetComponent<SpriteRenderer>().DOFade(1, transitionDuration).SetEase(easeType);

                float delay;

                if (weAreZoomingIn)
                {
                    delay = transitionDuration;
                }
                else
                {
                    delay = 0;
                }

                foreach (GameObject roomLabel in GameObject.FindGameObjectsWithTag("roomLabel"))
                {

                    roomLabel.GetComponent<TextMeshPro>().DOFade(1, transitionDuration/2).SetEase(easeType).SetDelay(delay);


                }

                foreach (GameObject control in GameObject.FindGameObjectsWithTag("volumeControl"))
                {
                    
                        control.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration).SetEase(easeType);


                }

                drawerIcon.GetComponent<SpriteRenderer>().DOFade(1,transitionDuration/2).SetEase(easeType).SetDelay(delay);
                iconPlayEverywhere.GetComponent<SpriteRenderer>().DOFade(0.125f, transitionDuration / 2).SetEase(easeType).SetDelay(delay);


                foreach (GameObject Object in GameObject.FindGameObjectsWithTag("object"))
                {
                    Object.GetComponent<SpriteRenderer>().DOFade(1, transitionDuration);
                    Object.transform.DOScale(new Vector3(0.38f, 0.38f, 0.38f), transitionDuration).SetEase(easeType);
                }

                //fade in playback toggles

                foreach (GameObject toggle in GameObject.FindGameObjectsWithTag("playbackToggle"))
                {
                    Transform t = toggle.transform;

                    for (int i = 0; i < t.childCount; i++)
                    {
                        t.GetChild(i).gameObject.GetComponent<SpriteRenderer>().DOFade(1, transitionDuration);

                        //and grandchildren :)
                        Transform tChild = t.GetChild(i).transform;

                        for (int j = 0; j < tChild.childCount; j++)
                        {
                            tChild.GetChild(j).gameObject.GetComponent<SpriteRenderer>().DOFade(1, transitionDuration);
                        }
                    }
                }

                //fade in any checkbox icons
                foreach (GameObject checkbox in GameObject.FindGameObjectsWithTag("checkmark"))
                {
                    checkbox.GetComponent<SpriteRenderer>().DOFade(1, transitionDuration / 2);
                }



                break;

            case 2: //ROOM//////////////////////////////////////////////////////////////////////////

                //print("SR = " + selectedRoom.name);

                iconPlayEverywhere.GetComponent<SpriteRenderer>().DOFade(0, 0.3f).SetEase(easeType);


                innerSphere.GetComponent<SphereCollider>().enabled = false;
                innerSphereBack.GetComponent<SphereCollider>().enabled = false;

                if (weAreZoomingIn == true)
                {
                    easeType = Ease.OutExpo;
                    transitionDuration = 0.8f;
                }
                else
                {
                    easeType = Ease.OutExpo;
                    transitionDuration = 0.6f;
                }

                GameObject.Find("Rooms").GetComponent<lookAtTheCamera>().enabled = false;
                //GameObject.Find("Rooms").transform.localEulerAngles = new Vector3(0, 0, 0);

                selectedRoom.GetComponent<PressGesture>().enabled = false;
                selectedRoom.GetComponent<ReleaseGesture>().enabled = false;

               

                innerSphere.SetActive(false);
                innerSphereBack.SetActive(false);

                mainCamera.transform.DOMove(new Vector3(selectedRoom.transform.position.x, selectedRoom.transform.position.y - 0.06f, selectedRoom.transform.position.z - cameraZOffset/2.2f), transitionDuration).SetEase(easeType);

                //fade out the other rooms

                foreach (GameObject room in GameObject.FindGameObjectsWithTag("room"))
                {
                    if (room != selectedRoom)   
                    {
                        room.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration).SetEase(easeType);
                        room.GetComponent<BoxCollider>().enabled = false;

                        //fade out any child objects in other rooms

                        Transform t = room.transform;

                        for (int i = 0; i < t.childCount; i++)
                        {
                            if (t.GetChild(i).gameObject.tag == "object")
                            {
                                t.GetChild(i).gameObject.GetComponent<SpriteRenderer>().DOFade(0, 0.3f);
                            }

                        }
                    }
                }

                bkgPlayEverywhere.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration).SetEase(easeType);

                foreach (GameObject Object in GameObject.FindGameObjectsWithTag("object"))
                {
                    
                    Object.transform.DOScale(new Vector3(0.2f, 0.2f, 0.2f), transitionDuration).SetEase(easeType);
                }

                //fade out any playback toggles that aren't in this room, or if they are, but they're switched off

                foreach (GameObject toggle in GameObject.FindGameObjectsWithTag("playbackToggle"))
                {
                    if (toggle.GetComponent<handlePlaybackToggle>().playbackIsEnabled == false || toggle.transform.parent != selectedRoom.transform)
                    {
                        Transform t = toggle.transform;

                        for (int i = 0; i < t.childCount; i++)
                        {
                            //fade out children
                            t.GetChild(i).gameObject.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration / 2);

                            //and grandchildren :)
                            Transform tChild = t.GetChild(i).transform;

                            for (int j = 0; j < tChild.childCount; j++)
                            {
                                tChild.GetChild(j).gameObject.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration / 2);
                            }


                        }
                    }

                    
                }

                //fade out all checkbox icons
                foreach (GameObject checkbox in GameObject.FindGameObjectsWithTag("checkmark"))
                {
                    checkbox.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration / 2);
                }







                //fade out the labels

                foreach (GameObject roomLabel in GameObject.FindGameObjectsWithTag("roomLabel"))
                {

                    roomLabel.GetComponent<TextMeshPro>().DOFade(0, transitionDuration).SetEase(easeType);


                }

                //fade out other volume sliders

                foreach (GameObject control in GameObject.FindGameObjectsWithTag("volumeControl"))
                {
                    if (control.transform.parent.transform.parent != selectedRoom.transform)
                    {
                        //print("no");
                        control.GetComponent<SpriteRenderer>().DOFade(0, transitionDuration).SetEase(easeType);

                    }
                    else
                    {
                        //print("yes");
                        control.GetComponent<SpriteRenderer>().DOFade(1, transitionDuration).SetEase(easeType);
                    }


                }


                break;
            case 3: //OBJECT//////////////////////////////////////////////////////////////////////////
                break;
            default:
                break;
        }

        labelTitle.DOFade(0, transitionDuration * 0.3f).OnComplete(changeTextTitleAndFadeIn);

        setGlobeAppearance();
    }

    void disableInnerSphere()
    {
        innerSphere.SetActive(false);
        innerSphereBack.SetActive(false);
    }

    void resetLookAtTheCamera()
    {

        //Invoke("disableInnerSphere", 0.2f);

        GameObject.Find("Rooms").GetComponent<lookAtTheCamera>().enabled = true;

    }

    //void OnApplicationQuit()
    //{

    //}

    void changeTextTitleAndFadeIn()
    {
        switch (zoomLevel)
        {
            case 0:
                labelTitle.text = "All Places";
                break;
            case 1:
                labelTitle.text = "Home";
                break;
            case 2:
                labelTitle.text = selectedRoom.name;
                break;
            case 3:
                labelTitle.text = "Spotify";
                break;
            default:
                break;
        }

        if (weAreZoomingIn == true)
        {
            labelTitle.DOFade(1, transitionDuration/2).SetDelay(transitionDuration * 0.7f);
            if (zoomLevel > 0)
            {
                labelBackButton.GetComponent<TextMeshPro>().DOFade(1, transitionDuration/2).SetDelay(transitionDuration * 0.7f);
            }
        }
        else
        {
            labelTitle.DOFade(1, transitionDuration/2).SetDelay(transitionDuration * 0.35f);

            if (zoomLevel > 0)
            {
                labelBackButton.GetComponent<TextMeshPro>().DOFade(1, transitionDuration/2).SetDelay(transitionDuration * 0.35f);

            }
        }

        

    }

    void setGlobeAppearance()
    {
        //disable touch gestures on the globe if we're not zoomed out to it.
        if (zoomLevel == 0)
        {
            
            globe.GetComponent<TransformGesture>().enabled = true;
        }
        else
        {
            globe.GetComponent<TransformGesture>().enabled = false;
        }

        

    }

    public void incrementZoomLevel()
    {
        weAreZoomingIn = true;

        if (zoomLevel < numberOfZoomLevels)
        {
            zoomLevel += 1;
        }

        setCameraPerZoomLevel();
    }

    public void decrementZoomLevel()
    {
        weAreZoomingIn = false;

        if (zoomLevel > 0)
        {
            zoomLevel -= 1;
        }

        setCameraPerZoomLevel();
    }
}
