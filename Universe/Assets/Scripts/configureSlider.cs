﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes2D;
using UnityEditor;

public class configureSlider : MonoBehaviour
{
    [Range(0, 360)]
    public int angleRange;
    public GameObject sliderRail;
    public GameObject sliderValueRail;

    // Start is called before the first frame update
    void Start()
    {
        
        setSliderAppearance();
    }

    void setSliderAppearance()
    {
        sliderRail.GetComponent<Shape>().settings.startAngle = 0;
        sliderRail.GetComponent<Shape>().settings.endAngle = angleRange;

        //sliderValueRail.GetComponent<Shape>().settings.startAngle = angleRange;
        //sliderValueRail.GetComponent<Shape>().settings.endAngle = 0;


        if (angleRange <= 180)
        {
            sliderRail.transform.localEulerAngles = new Vector3(0, 0, 90-angleRange/2);
            //sliderValueRail.transform.localEulerAngles = new Vector3(0, 0, 90 - angleRange / 2);
        }
        else
        {
            sliderRail.transform.localEulerAngles = new Vector3(0, 0, -270 - angleRange / 2);
           // sliderValueRail.transform.localEulerAngles = new Vector3(0, 0, -270 - angleRange / 2);
        }

    }

    private void OnValidate()
    {
        setSliderAppearance();
    }


}
