﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handlePlaybackToggle : MonoBehaviour
{

	public GameObject iconOff;
	public GameObject iconOn;
	public bool playbackIsEnabled = false;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void togglePlayback()
	{
		if (playbackIsEnabled == false)
		{
			playbackIsEnabled = true;
		}
		else
		{
			playbackIsEnabled = false;
		}

        setIconState();

    }

	public void setIconState()
	{
		if (playbackIsEnabled == true)
		{
			iconOff.SetActive(false);
			iconOn.SetActive(true);
		}
		else
		{
			iconOff.SetActive(true);
			iconOn.SetActive(false);
		}
   	}
}
